---
title: OMDb API (movie) scraper.
permalink: /blog/omdbapi-scraper/
categories:
  - News
  - Feature
summary:
  - image: release.png
---

With version 2.9.4 we provide a new movie scraper for the service [OMDb API](http://www.omdbapi.com/). This is a private scraper which needs a personal API key.

**tinyMediaManager offers a limited access to this service (10 calls per 15 seconds) - if you need more calls, consider becoming a patron of this service.**

You can obtain an API key if you become a patron (for details see [OMDb API](http://www.omdbapi.com/)) of this service. If you got a key, you can enter it into the scraper settings for the OMDb API scraper inside tinyMediaManager.
