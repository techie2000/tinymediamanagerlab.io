---
title: 10 years of tinyMediaManager
permalink: /blog/10-years-of-tinymediamanager/
categories:
  - News
summary:
  - image: tmm.png
---

tinyMediaManager was born in Summer 2012 as a hobby project by Manuel Laggner. After switching to Linux in 2012 he was missing a media manager to maintain his media library, so he started writing his own one. Inspired by other media managers, he initially tried to write a simple media manager for some basic tasks such as scraping movie data and artwork from [TMDB](https://www.themoviedb.org/). After several months of work, the first Alpha release of tinyMediaManager has been released on **October 14th 2012** in the [Kodi Forums](https://forum.kodi.tv/showthread.php?tid=142723).

Just a few days after releasing the first Alpha of tinyMediaManager, Manuel met Myron Boyle which joined the core tinyMediaManager team soon. Manuel and Myron develop tinyMediaManager for ten years now. In the last ten years, tinyMediaManager has grown from a "tiny" media manager to a full grown media management solution, containing 

- three modules (movies, movie sets/collections and TV shows)
- 12 metadata scrapers, 6 artwork scrapers, 4 trailer scrapers and 1 subtitle scraper
- a powerful renaming engine
- and much more

To get a feeling how much work went into this project, here are a few facts about tinyMediaManager:

- Amount of commits (just in the main branch): **9420**
- Amount of releases: **130**
- Lines of executable code: **166 000**
- Lines of code total: **254 000**
- Estimated cost (according to [Basic COCOMO Model](http://en.wikipedia.org/wiki/COCOMO)): **$ 2 950 000**

To visualize the evolution of tinyMediaManager, we've rendered our git activity with [gource](https://gource.io/).

{% include video id="EkkVsHyihSs" provider="youtube" %}

Thanks for reading this far - you deserve a special gift. We'll play some lottery and give away free licenses on [Reddit](https://www.reddit.com/r/tinyMediaManager/), so feel free to share and spread the word for the next few days!