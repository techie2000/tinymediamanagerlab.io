---
title: Version 4 and the future plans of tinyMediaManager
permalink: /blog/v4-future/
categories:
  - News
tags:
  - v4
summary:
  - image: tmm.png
---

tinyMediaManager is developed as Open Source since summer 2012 mainly by two developers (Manuel and Myron) in their spare time.

During this time the project grew to about 200.000 lines of code in over 7000 commits. According to the [COCOMO model](https://en.wikipedia.org/wiki/COCOMO) a calculation of the costs for this project results in over two million Euro.

Although this project is Open Source, the contribution to the development work of the tinyMediaManager core team is over 98%. On average we are working more than one hour per day for this project - in our spare time, for free. And all this without financial support or further help. Only the translations (in currently more than 30 languages) and the support in the forum is (partly) done by the community.

Since the lion's share of the work (development, maintenance, deployment, ...) remains within the tinyMediaManager team, we decided to charge an annual fee for the ongoing work on the project (starting with v4). This does not mean that the project itself will change much - tinyMediaManager remains open source and the code remains free.

Basically this means:

- tinyMediaManager remains Open Source - the code is still freely available (excluding our license module and the launcher)
- the compiled/packaged version of tinyMediaManager has some limitations feature wise. Not
all scrapers can be used, and some expert features are not available until unlocked by a 
license. But the basic functionality is still free.
- a license of tinyMediaManager can be purchased (planned is 10€) and will be valid for one year (because the maintenance will continue and new feature will be added constantly)

Since we support the Open Source idea, we will also reward contributions from the community with free licenses (be it translations, pull/merge requests, donations, help in the forum, testing of test versions, ...). Corresponding free licenses will be sent to contributors shortly.

We are aware that many users of tinyMediaManager will not like this decision, but we had the choice to either stop the development of tinyMediaManager or to continue the development and charge a fee. Unfortunately it is no longer economically viable for us to spend more than one hour per day for the maintenance of this project without financial compensation.

## What is new in v4?
tinyMediaManager v4 has some big changes as well as many fixes over v3:

+ bundled JRE and rewritten launcher (no installed Java needed any more)
+ real HiDPI support (slightly changed look and feel to achieve correct UI scaling)
+ universal TV show scraper
+ completely reworked libmediainfo usage:
  - full support of reading/writing mediainfo XML files
  - better support of disc structures (.iso and VIDEO_TS/BDMV/HVDVD_TS folders)
  - several fixes for reading data out of libmediainfo
+ enhanced filter management
+ new command line interface
+ mediainfo integration for Trakt.tv
+ support for Windows taskbar / macOS dock access
+ added a true regular expression search for movies/movie set/TV show titles
+ display missing episodes in the season overview panel
+ provide artwork from TMDB for Trakt.tv search
+ added showlink support for movies

## What happens with v3?
tinyMediaManager v3 will remain available and free, but only serious bugs will be fixed and no new features will be added. A mandatory upgrade to v4 will not be necessary, but we want to provide an easy transition for users which want to upgrade.

## When will this step be made?
We have been working on the finalization of v4 all summer and it is already in the starting blocks (nightly builds are already available). We hope that v4 can be released in the next weeks.
