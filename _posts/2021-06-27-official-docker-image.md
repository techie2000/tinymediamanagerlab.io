---
title: Official docker image
permalink: /blog/official-docker-image/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

tinyMediaManager offers an (experimental) [official docker image](https://hub.docker.com/r/tinymediamanager/tinymediamanager) based on Debian Buster (for maximum compatibility). This image includes all needed components (up to date libmediainfo, FFmpeg, ...) for the best experience. This image should work on any x86_64 device.

Using this image, you will get a fully working instance of tinyMediaManager which is accessible remotely (via web/VNC).

## Instructions

* Map any local port to `4000` for web access (via noVNC)
* Map a local volume/folder to `/data` where all local data from tinyMediaManager is stores (like `data`, `logs`, `cache` and `backups`)
* Map local volumes/folders/mounts to `/media/xxx` where all media will be accessed. E.g.:
  * /mnt/movies -> /media/movies
  * /mnt/tvshows -> /media/tvshows

## Sample run command:

```
docker run \
    --name=tinymediamanager \
    -p 4000:4000 \
    -v </path/to/local/data/>:/data \
    -v </path/to/movies>:/media/movies \
    -v </path/to/tvshows>:/media/tvshows \
    tinymediamanager/tinymediamanager:latest
```

After starting the container, you can access tinyMediaManager via `http://your-host-ip:4000`.

## Data volumes

You should map the following folders to a volume/folder on the host:

* `/data`: where all local data of tinyMediaManager will be stores. **If you do not map this out of the container, all your data/settings will be lost on an upgrade of tinyMediaManager**
* `/media`: contains all your data sources

You need read/write access to all mapped volumes!

## Configuration

This image offers some special configuration parameters which can be passed to docker by using [environment variables](https://docs.docker.com/engine/reference/commandline/run/#set-environment-variables--e---env---env-file)

| Variable&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; | &nbsp;&nbsp;Description&nbsp;&nbsp; | &nbsp;&nbsp;Default&nbsp;&nbsp; |
| :--- | :---: | :---: |
| USER_ID | ID of the user tinyMediaManager runs as. See below for an example how to use this. | 1000 |
| GROUP_ID | ID of the group tinyMediaManager runs as. See below for an example how to use this. | 1000 |
| PASSWORD | Password for remote access | |


## Permissions

When using using data volumes (-v flags for `/data` and `media`), permissions issues can occur between the host and the container (tinyMediaManager). For example, the user within the container may not exist on the host or does not match the _right_ user on the host. This could prevent the container from properly accessing files and folders on the shared volume and vice versa. To avoid any problem, you can specify the user tinyMediaManager should run as.

This is done by passing the user ID and group ID to the container via the `USER_ID` and `GROUP_ID` environment variables.

To find the right IDs to use, issue the following command on the host, with the user owning the data volume on the host:

```
id <username>
```

Which gives an output like this one:

```
uid=1000(myuser) gid=1000(myuser) groups=1000(myuser),4(adm),24(cdrom),27(sudo),46(plugdev),113(lpadmin)
```

The value of `uid` (user ID) and `gid` (group ID) are the ones that you should be given the container.

## Clipboard

Because of security restrictions in the browser, your client clipboard cannot be synchronized to the clipboard of tinyMediaManager. To copy data from/to the remote connection of tinyMediaManager, you need to invoke the [clipboard feature](https://novnc.com/img/noVNC-5-clipboard.png) from noVNC. Just open the control panel of noVNC from the left side of the window and use the clipboard feature from there to copy the data.

