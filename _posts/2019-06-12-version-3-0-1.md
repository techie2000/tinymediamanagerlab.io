---
title: Version v3.0.1
permalink: /blog/version-3-0-1/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
---
x fixed Fanart.tv reading personal clientKey  
x fixed Fanart.tv using TMDB id fallback  
x fixed TvShow exporter naming pattern #525  
x fixed TvShow renamer (Synology TvShow metadata overwriting videos) #527  
x fixed scraping of episode cast with IMDB  
x fixed displaying of episode director/writers in the episode editor  
x fixed downloading of season artwork #522  
x removed VIDEO_TS/BDMV NFO styles (integrated these into the other two styles)  
x updated JNA and libmediainfo  
x do not print "Unknown" for empty media sources  
x fixed artwork fetching with tvdb and removed bad thumb urls from the DB (you may need to re-write your NFO files)  
x fixed occasional Java crashes on OSX  
x fixed occasional error popup on start of tinyMediaManager  
