---
title: Version v4.1.3
permalink: /blog/version-4-1-3/
categories:
  - News
  - Release
tags:
  - v4
summary:
  - image: release.png
---
\+ Emby compatibility enhancements  
  - (TV shows) proper trailer naming for Emby  
  - (TV shows) added tmdbid to the NFO  
  - (TV shows) added option to write \<enddate\> to the TV show NFO  
  - (TV shows) added option to write all actors/guests into the TV show NFO  

\+ (TV shows) added vote count column  
\+ basic support for nextpvr recording XML parsing #1252  
x added shutdown hook to prevent database corruption  
x (TV shows) also fetch IMDB ratings for episodes  
x (movies) catch occasional errors in movie set sorting  
x check write access to the tinyMediaManager folder on startup  
x (TV shows) added columns for music theme and video bitrate  
x (TV shows) do not mix in a missing episode on media file exchange  
x (movies/TV shows) fixed creation of trailer folder while muxing  
x (TV show) fixed bug when writing TV show NFO  
x re-aligned new rating logos to do not take up that much vertical space  
x (TV show) prevent rating panel from flickering  
x do not draw the rating panel over popups #1265  
x change filter indicator if all filters are deactivated #1268