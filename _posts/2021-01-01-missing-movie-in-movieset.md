---
title: Missing Movies in Movie Set
permalink: /blog/missing-movies-in-movieset/
categories:
  - News
tags:
  - v4
  - v4.1
summary:
  - image: tmm.png
---

tinyMediaManager v4.1 will include the ability to display missing movies from movie sets (like the missing episode feature for TV shows). This can only be achieved for movie sets which are available at The Movie Database (TMDB).

To activate this feature, you have to

1. Activate the corresponding setting (Settings -> Movie sets -> Display missing movies)
2. Re-scrape the movie sets to store the information (there is now also an extra action to achieve this -> right click on a movie set and choose "Get missing movies for selected movie set(s). Please note that this only works for movie sets, which do have a TMDB id associated).

**Note**: The meta data (TMDB id, missing movies, ...) for movie sets cannot be stored outside of tinyMediaManager and is _lost_ if you rebuild your tinyMediaManager database. If you need to rebuild the tinyMediaManager database, you need to re-scrape your movie sets to get the proper ids and missing movie information.
{: .notice--primary}