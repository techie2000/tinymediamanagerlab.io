---
title: Version v3.0
permalink: /blog/version-3-0/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
excerpt: After several years of development we are proud to announce tinyMediaManager v3...
---
After several years of development we are proud to announce

# tinyMediaManager v3

available at [https://www.tinymediamanager.org/download/](https://www.tinymediamanager.org/download/).

key changes are:
+ completely rewritten UI (thx to @joostzilla)
  - new style and layout (better usage of the available space
  - especially for low screen devices)
  - flexible and configurable tables
  - improved filters (inclusive and exclusive filters)
  - better UI scaling on high dpi screens
+ added a dark theme
+ completely rewritten the renamer engine
+ completely rewritten NFO parsing and writing (much more flexible now)
+ easier translation via [weblate.org](https://hosted.weblate.org/projects/tinymediamanager/) now
+ increased required Java version to Java 8+
+ ability to mix in missing episodes
+ presets in settings for common media centers
+ many enhancements under the hood


since many things have changed, you are not able to directly upgrade from an existing v2 version of tinyMediaManager! tinyMediaManager v2 offers you a guided upgrade to v3 (automatic download/patch and an automatic migration of your data source), but you have to re-set all your other settings.

Please report any issues/feature requests at [our project home](https://gitlab.com/tinyMediaManager/tinyMediaManager/).
