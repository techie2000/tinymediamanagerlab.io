---
title: Version v3.0 RC1
permalink: /blog/version-3-0-RC1/
categories:
  - News
  - Release
tags:
  - v3
summary:
  - image: release.png
excerpt: After several years of development we are proud to announce tinyMediaManager v3 - RC1...
---
After several years of development we are proud to announce

# tinyMediaManager v3 - RC1

available at [https://prerelease.tinymediamanager.org/](https://prerelease.tinymediamanager.org/).

key changes are:
+ completely rewritten UI (thx to @joostzilla)
  - new style and layout (better usage of the available space
  - especially for low screen devices)
  - flexible and configurable tables
  - improved filters (inclusive and exclusive filters)
  - better UI scaling on high dpi screens
+ added a dark theme
+ completely rewritten the renamer engine
+ completely rewritten NFO parsing and writing (much more flexible now)
+ easier translation via [weblate.org](https://hosted.weblate.org/projects/tinymediamanager/) now
+ increased required Java version to Java 8+
+ ability to mix in missing episodes
+ presets in settings for common media centers
+ many enhancements under the hood


since many things have changed, you are not able to upgrade from an existing v2 version of tmm! You need to download tmm v3 from [https://prerelease.tinymediamanager.org/](https://prerelease.tinymediamanager.org/) and create a separate installation with the new settings for v3!

Please report any issues/feature requests at [our project home](https://gitlab.com/tinyMediaManager/tinyMediaManager/).
