---
title: Version 2.9.14
permalink: /blog/version-2-9-14/
categories:
  - News
  - Release
tags:
  - v2
summary:
  - image: release.png
---
\+ Java 9 / 10 / 11 compatibility!  
\+ High Dynamic Range detection for mediafiles - you need to reload MediaInfo and then use $H in renamer  
\+ option to ignore SSL certificate problems #373  
\+ make colon replacement configurable (for movies & TV shows) #396  
x retrying to get artwork for 5 times #369  
x fixed detection of language names  
x fixed scraping of OFDB  
x fixed language name for sl/sk  
x CMD export: sorted movies/TV shows the same way as in UI mode  
x fixed detection of nested extras folders  
x fixed detection of extras/extrafanart/extrathumbs in TV show update datasources #395  
x only set 3D flag/edition on first import #231  
x fixed statusbar layout with long texts  
x TMDB: fixed fallback language scraping for episodes  
x fixed subtitle search dialog  
x fixed Trakt.tv, updating local NFOs even when no changes  
x fixed IMDB release date parsing  
x allow moving of symlinks #410  
x fixed crash on weird language extensions #425  
x fixed MovieSets #366  
