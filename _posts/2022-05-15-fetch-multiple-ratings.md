---
title: Fetch Multiple Ratings
permalink: /blog/fetch-multiple-ratings/
categories:
  - News
tags:
  - v4
  - v4.3
summary:
  - image: tmm.png
---

tinyMediaManager v4.3 introduces a new way to get multiple ratings at once. Up to now we could only offer the rating from the scraper itself along with IMDb rating (if the fetched movie/TV show/episode metadata contains an IMDb id).

The new way of fetching multiple ratings can be used in two different ways:

## 1. Automatically fetch all available ratings after scraping

To let tinyMediaManager automatically fetch all ratings directly after scrape, you just need to activate the option **Fetch ratings from other sources too** in the _Scraper options_ of the movie/TV show settings:

<a class="fancybox" href="{{ site.urlimg }}2022/05/fetch-all-ratings-settings.png" rel="post" title="Fetch all ratings option">![Fetch all ratings option]({{ site.urlimg }}2022/05/fetch-all-ratings-settings.png "Fetch all ratings option"){: .align-center}</a>

After enabling this option, tinyMediaManager fetches all ratings directly after scraping with the chosen scraper.

## 2. Download ratings

You can also download ratings with the action **Fetch ratings for selected movie(s)** / **Fetch ratings for selected Tv show(s)/episode(s)** which is located in either the **Edit** menu on the toolbar or in the **Enhanced editing** submenu of the context menu. By triggering this action you will be prompted by a dialog where you can choose which ratings you want to fetch:

<a class="fancybox" href="{{ site.urlimg }}2022/05/fetch-all-ratings-dialog.png" rel="post" title="Fetch all ratings dialog">![Fetch all ratings dialog]({{ site.urlimg }}2022/05/fetch-all-ratings-dialog.png "Fetch all ratings dialog"){: .align-center}</a>