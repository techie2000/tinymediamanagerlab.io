---
title: TheTVDB API v4
permalink: /blog/tvdb-api-v4/
categories:
  - News
tags:
  - v4
  - v4.2
summary:
  - image: tmm.png
---

[TheTVDB](https://www.thetvdb.com/) announced that the sunset of their (free) API v3 will be on October 1, 2021. With the sunset of TheTVDB API v3, the current implementation in tinyMediaManager will no longer be able to pull data from TheTVDB.

TheTVDB API v4 is not free anymore (you can use it either via a subscription or by contributing to TheTVDB) - **but we managed to negotiate a contract to make it possible to use TheTVDB without any additional subscription**.

Starting with tinyMediaManager v4.2 the implementation of TheTVDB API v4 is included with free access to TheTVDB for all **PRO** users. Users of tinyMediaManager v2, v3, v4.0 and v4.1 will lose the ability to use TheTVDB after October 1, 2021.




