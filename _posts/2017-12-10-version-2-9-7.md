---
title: Version 2.9.7
permalink: /blog/version-2-9-7/
categories:
  - News
  - Release
tags:
  - v2  
summary:
  - image: release.png
---
\+ added DataTables export template - thx to @C. Janke  
x fixed the imdb scraper (imdb changed their web pages) - thx to @Grizzler89  
x fixed a parsing problem in the Kodi scraper  
x fixed movie renamer with animated posters  
