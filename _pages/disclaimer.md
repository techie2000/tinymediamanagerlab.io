---
permalink: /disclaimer/
title: "Disclaimer"
author_profile: true
---
# Disclaimer

The materials on this website are provided on an 'as is' basis. The author makes no warranties, expressed or implied, and hereby disclaims and negates all other warranties including, without limitation, implied warranties or conditions of merchantability, fitness for a particular purpose, or non-infringement of intellectual property or other violation of rights.

Further, the author does not warrant or make any representations concerning the accuracy, likely results, or reliability of the use of the materials on its website or otherwise relating to such materials or on any sites linked to this site.

The materials appearing on this website could include technical, typographical, or photographic errors. The author does not warrant that any of the materials on its website are accurate, complete or current. The author may make changes to the materials contained on its website at any time without notice.

The author has not reviewed all of the sites linked to its website and is not responsible for the contents of any such linked site. The inclusion of any link does not imply endorsement by the author of the site. Use of any such linked website is at the user's own risk.
