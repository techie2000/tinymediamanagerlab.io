---
title: Nightly Build download
layout: single
classes: wide

permalink: /download/nightly-build/

sidebar:
  nav: "downloads"
---

Nightly builds are automatically built every day and should only be used for hunting down bugs on **test files**. These build may contain severe bugs which can delete/destroy your test files! distribution too).

You can download and evaluate tinyMediaManager for free (with some limitations. [More details >>](/purchase/)).

<iframe src="https://nightly.tinymediamanager.org/download_v4.html" style="width:100%; height:850px;border:none;">
<p>You find the latest nightly builds at <a href="https://nightly.tinymediamanager.org">https://nightly.tinymediamanager.org</a></p>
</iframe>

<br />
All downloads are available at [https://nightly.tinymediamanager.org](https://nightly.tinymediamanager.org).
