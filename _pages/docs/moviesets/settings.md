---
layout: single
permalink: "/docs/moviesets/settings"
title: "Movie Set Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

## Movie Sets ##

### UI Settings ###

* **Persist UI filters**: remember the last active UI filter (this will be loaded on the next start of tinyMediaManager)
* **Display missing movies**: activating this option will trigger tinyMediaManager to display missing movies for movie sets where this information is available
* **Movie set table tooltips**: using this setting you can activate/suppress tooltips in the movie set table

### Local movie set data ###

* **Movie set data folder**: since movie sets do not have a dedicated location on your storage, you need to specify a folder where to store movie set data (such as metadata/NFO and artwork)
* **NFO format**: the NFO format how you want to store movie set data
* **NFO file naming**: the file name which to use when writing movie set NFO files:
  * \<movie set name>/\<movie set name\>.nfo
  * \<movie set name\>.nfo
  * \<movie set name>/collection.nfo

### Misc. Settings ###
* **Metadata completeness**: here you can choose which metadata fields will be checked for the green checkmark in the movie set table for completeness
* **Artwork completeness**: here you can choose which artwork files will be checked for the green checkmark in the movie set table for completeness

## Artwork ###

* **Automatically scrape images without selection**: with this option enabled, tinyMediaManager tries to find the best image files from the artwork scrapers according to your settings in the _Images_ section.

You can store the artwork in three different ways:

- Movie folder style: `<movie folder>/movieset-<artwork type>.ext`
- Kodi/Artwrok Beef style: `<movie set artwork folder>/<movie set name>/<artwork type>.ext`
- Movie Set Artwork Automator style: `<movie set artwork folder>/<movie set name>-<artwork type>.ext`

`.ext` stands for the image format of the artwork file which should be either `.png`, `.jpg` or `.gif`.

The following artwork types and filenames are available:

* **Poster**
  * \<movie folder\>/movieset-poster.ext
  * \<movie set artwork folder\>/\<movie set name\>/poster.ext
  * \<movie set artwork folder\>/\<movie set name\>-poster.ext
* **Fanart**
  * \<movie folder\>/movieset-fanart.ext
  * \<movie set artwork folder\>/\<movie set name\>/fanart.ext
  * \<movie set artwork folder\>/\<movie set name\>-fanart.ext
* **Banner**
  * \<movie folder\>/movieset-banner.ext  
  * \<movie set artwork folder\>/\<movie set name\>/banner.ext
  * \<movie set artwork folder\>/\<movie set name\>-banner.ext
* **Clearart**
  * \<movie folder\>/movieset-clearart.ext  
  * \<movie set artwork folder\>/\<movie set name\>/clearart.ext
  * \<movie set artwork folder\>/\<movie set name\>-clearart.ext
* **Thumb**
  * \<movie folder\>/movieset-thumb.ext  
  * \<movie folder\>/movieset-landscape.ext  
  * \<movie set artwork folder\>/\<movie set name\>/thumb.ext
  * \<movie set artwork folder\>/\<movie set name\>/landscape.ext
  * \<movie set artwork folder\>/\<movie set name\>-thumb.ext
  * \<movie set artwork folder\>/\<movie set name\>-landscape.ext
* **Logo**
  * \<movie folder\>/movieset-logo.ext  
  * \<movie set artwork folder\>/\<movie set name\>/logo.ext
  * \<movie set artwork folder\>/\<movie set name\>-logo.ext
* **Clearlogo**
  * \<movie folder\>/movieset-clearlogo.ext    
  * \<movie set artwork folder\>/\<movie set name\>/clearlogo.ext
  * \<movie set artwork folder\>/\<movie set name\>-clearlogo.ext
* **Disc art**
  * \<movie folder\>/movieset-disc.ext    
  * \<movie folder\>/movieset-discart.ext
  * \<movie set artwork folder\>/\<movie set name\>/disc.ext
  * \<movie set artwork folder\>/\<movie set name\>/discart.ext
  * \<movie set artwork folder\>/\<movie set name\>-disc.ext
  * \<movie set artwork folder\>/\<movie set name\>-discart.ext
