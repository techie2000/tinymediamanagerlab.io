---
layout: single
permalink: "/docs/toolbar"
title: "Toolbar"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"

gallery1:
  - url: images/screenshots/v3/toolbar.png
    image_path: images/screenshots/v3/toolbar.png

---

The main functions of tinyMediaManager are accessible via the toolbar. This toolbar is divided into two parts:

- the functions for the actual module (left hand side)
- common functions (right hand side)

{% include gallery id="gallery1" %}

The functions for the modules may differ in the detail, but their basic meaning is the same. The icon itself is a button executing the default function on click whereas the text beneath the button conatins a menu with all secondary functions.

1. **Update sources**: Update the specified data sources from the settings to find new media (or cleanup externally changed file/folder structures). This is used if you add new media to tinyMediaManager or if you have moved/deleted files/folders outside of tinyMediaManager.
2. **Search & Scrape**: Search/scrape the selected media entities from online data providers.
3. **Edit**: Edit the selected media entities
4. **Rename & Cleanup**: Rename and cleanup the selected media entities according to the renamer settings.

On the right hand side there are common functions which will stay the same for all modules:

1. **Settings**: Show the settings dialog
2. **Tools**: Quick access to several tools in tinyMediaManager like clearing various caches, show logs/messages, access t0 Wake on Lan devices, Reports bugs...
3. **Info**: All available information of tinyMediaManager like access to frequently asked questions, our Wiki, our Forums, the changelog, our bug report system, our homepage and an about page.
4. **Donate**: You can also donate to tinyMediaManager.
