---
layout: single
permalink: "/docs/movies/settings"
title: "Movie Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

## Movies ##

### Media Center Presets ###

tinyMediaManager has presets for various media centers. By pressing the button for your desired media center, all affected settings will be set to the default for the chosen media center.

Currently we support presets for:

* Kodi (17+)
* XBMC/Kodi (<17)
* Jellyfin
* Emby
* Plex
* MediaPortal 1.x (Moving Pictures and MyVideo)
* MediaPortal 2.x

### Automatic Tasks ###

* **Automatic rename**: you can let tinyMediaManager automatically rename your movies after they have been scraped
* **Automatic aspect ratio detection**: tinyMediaManager automatically detects the aspect ratio of movies after they have been scraped
* **Automatic sync with Trakt.tv**: if you have enabled access to Trakt.tv, you are able to automatically sync your media library and watched state with Trakt.tv. Is that option is disabled, you have to manually trigger the synchronization process
* **Automatic update on startup**: you can let tinyMediaManager update all your movie data sources on startup

### Misc. Settings ###

* **Extract meta data (via mediainfo) on "update data sources"** if no NFO is available: if there is no NFO available you can let mediainfo try to extract embedded metadata from your movie files
* **Extract missing artwork from VSMETA files on "update data sources"**: VSMETA files can contain artwork. By activating this option, tinyMediaManager extracts missing artwork directly from the VSMETA files when scanning for new content
* **Build image cache on import**: when importing new movies into tinyMediaManager you can create the image cache on the fly (only available when the image cache is enabled in the general settings). **Caution**: building the image cache will take a while
* **Prefer runtime from media info**: when activating this setting, the movie runtime is being taken directly from your movie file rather than the scraper
* **Include external audio streams in NFO**: using this setting tinyMediaManager will add external audio streams to the `streamdetails` section in the NFO files

## UI Settings ##

### UI Settings ##

* **Show the following artwork types in the detail view**: to choose which kind of artwork should be displayed in the movie detail tab
  * Poster
  * Fanart
  * Banner
  * Thumb 
  * Clearlogo
* **Show tooltips in the movie table**: enable/disable of tooltips in the movie table
* **Completeness checks**: there are several options to fine tune the completeness check (metadata and artwork) in the movie table
  * **Always display missing fields in the tooltip**: you can show all missing fields in the tooltip in addition to the completeness indicator (which only checks the chosen fields)
* **Preferred rating**: tinyMediaManager supports multiple ratings for every movie. The rating order in this setting indicates which rating source should be preferred for being used in the UI.

### Filters ###

* **Movie quickfilter**: to choose which data of the movie should be used when searching in the filter box (above the movie table). Possible fields to search at the moment:
  * Title
  * Title sortable (the same as the title, but with the prefix at the end)
  * Original title
  * Original title sortable (the same as the original title, but with the prefix at the end)
  * Sorttitle
* **Universal text filter**: in addition to the quickfilter (above the movie table), there is an universal text filter in the filter dialog. You can choose which fields will be used for the universal filter 
* **Persist UI filters**: remember the last active UI filter (this will be loaded on the next start of tinyMediaManager)

## Data Sources ##

### Data Sources ###

This is the core setting of the movie section. Every folder specified in this list will be searched for movie files. Only movies file *in and beneath* this folder will be found by the _update data sources_ action. Every movie _should_ be in its own folder, but **having multiple movies per folder is also supported, but will deactivate some features!** tinyMediaManager (and Java) supports accessing local drives and network shares, but is unable to perform mount/logon actions. You have to connect to external drives from your system **before** starting an _update data sources_ in tinyMediaManager!

To achieve the best possible experience, make sure you have a similar setup (e.g. H:\movies is one data source set in the settings):

```
H:\movies
    Aladdin
        Aladdin.avi
        movie.nfo
        sample
            sample.avi
    Alice in Wonderland
        Alice.in.Wonderland.mkv
        movie.nfo
        fanart.jpg
        poster.jpg
    Cars
        Cars.avi
    ...
```

### Exclude Folder(s) from Scan ##

Every folder in this list will be excluded from a scan. You can also put a file called `.tmmignore` into every folder you wish to have excluded from the scan. Additionally you can use regular expressions here to exclude folders from the scan.

### Bad Words ###

Words from this list will be removed from file names while parsing for the title. This comes in handy if you some special words like release groups in your file names.

## NFO Settings ##

* **NFO format**: There is support several different NFO formats. Choosing the right format affects how the data is written to the NFO files. You will find all compatible NFO formats [here](/docs/movies/nfo-formats).
* **NFO file naming**: You can choose between different file names for the NFO files. If no NFO file name is selected, tinyMediaManager does not write any NFO files.
  * \<movie_filename\>.nfo
  * movie.nfo (only works within "one movie per folder" scheme)
* **Write NFO files inside disc folders (BDMV, VIDEO_TS)**: Kodi needs to have NFO file inside disc folders (BDMV, VIDEO_TS), but other tools may need them outside of that folder. You can choose whether to write the NFO files into or outside of disc folders.
* **Write clean NFO**: If this option is activated, tinyMediaManager write a _clean_ NFO file without embedding _unknown/unsupported_ data from existing NFO files.
* **Source for \<dateadded\>**: there are several _sources_ where you can retrieve the date added for your movies. This option lets you specify which one will be written into the NFO.
* **NFO language**: The language in which localized texts like genre names should be written to the NFO file.
* **Certification format**: The format of the data in the \<certification\> tag.
* **Create outline tag from the plot**: tinyMediaManager does not support any outline internally, but some media centers need an outline tag. By activating this setting tinyMediaManager generates an outline tag via the plot.
* **Create outline from the first sentence of the plot**: Rather than using the whole plot, this will generate the outline from the first sentence of the plot.
* **Only include the first studio**: Some Kodi skins only support one studio in the NFO to display the studio logo. With this setting tinyMediaManager will only include the first studio into the NFO.
* **Add \<lockdata\>true\</lockdata\> to the NFO**: When writing NFO files with this tag, (at least) Emby is being prevented from modifying tinyMediaManager created NFO files

## Scraper ##

In this list you can choose your preferred metadata scraper from a list of all available scrapers. At the moment tinyMediaManager offers scrapers for the following meta data sources:

* themoviedb.org
* imdb.com
* thetvdb.com
* omdbapi.com
* moviemeter.nl
* trakt.tv
* ofdb.de
* mpdb.tv

And additionally there are two meta scrapers which offer enhanced data:

* **Universal scraper**: with this scraper you can combine results from various other scrapers to create an individual scraping result for your needs
* **Kodi scraper**: this meta scraper is able to parse scrapers from Kodi to embed them into tinyMediaManager. This scraper searches for local installed Kodi instances to use the scrapers from Kodi.

You will find a detailed description of those scrapers on the [scraper description page](/docs/scrapers).

## Scraper Options ##

### Advanced Options ###

* **Preferred language**: Choose the preferred language for scraping (for localized content like title, tagline and plot). Not all scrapers offer localized content, but tinyMediaManager tries to find localized in this language.
* **Certification country**: Movie certifications are available for several countries. You can choose for which country tinyMediaManager should try to get the certification.
* **Country for release date**: Release dates can vary in different countries. You can choose which country to prefer when scraping release dates.
* **Fall back to other scrapers**: If nothing has been found with your preferred scraper, try to search with other scrapers
* **Capitalize first letter of every word in title and original title**: As the option itself tells - when activating this, tinyMediaManager puts the found title/original title to *Title Case*.

### Metadata Scrape Defaults ###

In this section you can set which types of metadata should be scraped per default. You can always override this in the scrape dialogs in tinyMediaManager.

The command line version of tinyMediaManager completely relies on this setting.

### Artwork ###

* **Automatically scrape images**: with this option enabled, tinyMediaManager tries to find the best image files from the artwork scrapers according to your settings in the _Images_ section.

### Automatic Scraper ###

When doing an automatic scrape, tinyMediaManager calculates a score of every found movie. 1.0 means the search term and the search result are 100% the same whereas 0.0 means they are absolutely different. With this setting you can set the threshold starting of which score tinyMediaManager should take the found movie.

## Artwork ##

### Artwork Scraper ###

In this list you can choose the artwork scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible artwork. At the moment tinyMediaManager offers scrapers for the following artwork sources:

* themoviedb.org
* fanart.tv
* imdb.com
* FFmpeg
* mpdb.tv

### Advanced Options ###

* **Preferred language**: Get the artwork with text in the given language. `-` indicates that tinyMediaManager should look for artwork without any texts on it. Please note that not all artwork sources offer the needed information for this to work (it is known that themoviedb.org and fanart.tv work with this setting).
* **Poster size**: Choose the preferred poster size. tinyMediaManager will try to get the poster in this size (and the chosen language). If it’s not available an image from the other sizes will be taken.
* **Fanart size**: Choose the preferred fanart size. tinyMediaManager will try to get the fanart in this size (and the chosen language). If it’s not available an image from the other sizes will be taken.
* **Prefer language over resolution when choosing images**: If the desired artwork size has not been found in the desired language, take a lower size image with the preferred language rather than a bigger artwork in another language

## Artwork Filenames ##

Generally speaking artwork filenames either take the movie filename as a base an postfixes it with their type or use some filename independent naming scheme. Here you will find an example how this works:

| Option | Description | Example filename (Aladdin.mkv) |
|--------|-------------|-----------------------|
|\<movie filename\>.ext | movie filename + extension of the original image file | Aladdin.jpg |
|\<movie filename\>-poster.ext | movie filename + -poster + extension of the original image file | Aladdin-poster.jpg  |
|poster.ext | poster + extension of the original image file | poster.jpg |
|folder.ext | folder + extension of the original image file | folder.jpg |
|movie.ext | movie + extension of the original image file | movie.jpg |

`.ext` stands for the image format of the artwork file which should be either `.png`, `.jpg` or `.gif`.
{: .notice--warning}

The following artwork types and filenames are available:

* **Poster**
  * \<movie filename\>-poster.ext
  * \<movie filename\>.ext
  * poster.ext
  * movie.ext
  * folder.ext
  * cover.ext
* **Fanart**
  * \<movie filename\>-fanart.ext
  * \<movie filename\>.fanart.ext
  * fanart.ext
* **Banner**
  * \<movie filename\>-banner.ext  
  * banner.ext
* **Clearart**
  * \<movie filename\>-clearart.ext  
  * clearart.ext
* **Thumb**
  * \<movie filename\>-thumb.ext  
  * thumb.ext
  * \<movie filename\>-landscape.ext  
  * landscape.ext
* **Logo**
  * \<movie filename\>-logo.ext  
  * logo.ext
* **Clearlogo**
  * \<movie filename\>-clearlogo.ext    
  * clearlogo.ext
* **Disc art**
  * \<movie filename\>-disc.ext    
  * disc.ext
  * \<movie filename\>-discart.ext  
  * discart.ext
* **Keyart**
  * \<movie filename\>-keyart.ext  
  * keyart.ext

**BE AWARE**: All movie filename independent artwork options only work for single movie folders! Whenever tinyMediaManager detects that your movie is in a multi movie folder, it will fall back to `<movie filename>-<type>.ext` (Aladdin-poster.jpg for example).
{: .notice--warning}

## Extra artwork ##

* **Enable extrathumbs**: This option allows you to store multiple thumbs to the `extrathumbs` folder inside the movie folder. Only works for movies in dedicated folders.
  * **Resize extrathumbs**: With this option you are able to scale down the extrathumbs to a given width.
  * **Maximum of downloaded images**: In addition to choose the extrathumbs by hand (image chooser dialog) you can automatically download extrathumbs. This option let you choose how much extrathumbs will be downloaded per movie.
* **Enable extrafanarts**: This option allows you to store multiple fanarts inside the movie folder. There are four different ways to store extrafanart files in the movie folders. Be aware that only the style _with_ with movie name in it will work for multi movie folders!
  * `<movie filename>-fanartX.ext`
  * `<movie filename>.fanartX.ext`
  * `fanartX.ext`
  * `extrafanart/fanartX.ext`

  `fanartX.ext` and `extrafanart/fanartX.ext` only works in folders with only one movie
  {: .notice--warning}

  * **Maximum of downloaded images**: in addition to choose the extrafanarts by hand (image chooser dialog) you can automatically download extrafanarts. This option let you choose how much extrafanarts will be downloaded per movie.
* **Download actor images to .actors**: Kodi supports reading of actor images from the (hidden) folder `.actors` inside the movie folder. If you enable this option, all available actor images will be downloaded to this folder upon scraping the movie.

## Trailer ##

In this list you can enable all wanted trailer scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible trailers. At the moment tinyMediaManager offers scrapers for the following trailer sources:

* themoviedb.org
* hd-trailers.net
* davestrailerpage.co.uk
* ofdb.de

### Advanced Options ###

* **Use preferred trailer settings**: Rather than choosing the first available trailer, you can set your preferred trailer source (e.g. Youtube) and trailer quality (e.g. 1080p).
* **Automatic trailer download**: Upon scraping, also download the chosen trailer to your movie folder if no local trailer has been found
* **Trailer file naming**: You can set the desired trailer filename. If no desired filename is set, no trailer will be downloaded.
  * \<movie filename\>-trailer.ext
  * movie-trailer.ext
  * trailers/movie-trailer.ext

`movie-trailer.ext` only works in folders with only one movie
{: .notice--warning}

## Subtitles ##

In this list you can enable all wanted subtitle scrapers from a list of all available scrapers. You can activate multiple scrapers here to get the best possible subtitles. At the moment tinyMediaManager offers scrapers for the following subtitle sources:

* opensubtitles.org

### Advanced Options ###

* **Preferred language**: The preferred language for subtitle download.
* **Force downloading of best subtitle**: If there is no match by the file hash, try to download the best-matching subtitle
* **Subtitle language style**: Save subtitle files with the given language style in their filename.
  * **Subtitles without language tag**: You can name your subtitles without any language tag in the file name, **BUT** this will only work if there is exactly **ONE** subtitle file

## Renamer ##

tinyMediaManager offers a powerful renamer to rename your movies and all associated files to your desired folder-/filenames. While there is almost nothing you can't do with the renamer, it has still one big restriction: you can only rename the movies inside its own data source. Renaming it to a destination which is not inside the own data source is not supported.

### Renamer Pattern ###

* **Folder name** and **Filename**: Choose the desired folder name and filename for renaming. Here you can enter fixed parts of the name and dynamic ones. You will find a list of all available tokens for building up dynamic names beneath the settings along with examples of your media library. With leaving the folder name/filename empty, the renamer will skip the name generation for the empty part.

You will find more details of the movie renamer in the [corresponding docs](/docs/movies/renamer).

### Advanced Options ###

* **Automatic rename**: you can let tinyMediaManager automatically rename your movies after they have been scraped.
* **Replace spaces in folder name** and **Replace spaces in filename**: You can replace all spaces with either underscores, dots or dashes by activation this option.
* **Replace colons with**: Since colons are illegal characters in folder names and filenames, tinyMediaManager offers you to choose how they should be replaced.
* **Replace non ASCII characters with their basic ASCII equivalents**: Some file systems might need to have ASCII conform file names - by activating this option tinyMediaManager tries to convert non ASCII characters into a similar ASCII character (e.g. Ä - Ae; ß - ss, ...)
* **First character number replacement**: If you use the renderer `;first` (like in `${title;first}`) and the first character would be a digit, replace the digit with the given character.
* **Enable movie set tokens also for movie sets containing only one movie**: Unless this is activated, the renamer only creates a movie set folder if there is more than one movie in the set.
* **Remove all other NFOs**: Remove all non tinyMediaManager NFOs when renaming (clean up)   
* **Allow renaming movies into existing/other movie folders**: _DANGER_ - this allows you to rename movies into existing folders (which may contain other movies). This can mess up your folder structure

### Example ###

Here you can see the renamer results using an example from your library.

## Post processing ##

You can trigger external tools from within tinyMediaManager to do some work, which is not doable in tinyMediaManager itself. In this section you are able to maintain the access to different external tools along with all needed parameters. To access data from your movies which may be passed to the external tools, you can use [JMTE](/docs/jmte) syntax.

You will find more details about post processing in the [corresponding docs](/docs/post-processing).