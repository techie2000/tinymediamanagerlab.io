---
layout: single
permalink: "/docs/movies/moviepanel"
title: "Movie Panel"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"

gallery1:
  - url: images/docs/movies/movies01.png
    image_path: images/docs/movies/movies01.png
gallery2:  
  - url: images/docs/movies/movies02.png
    image_path: images/docs/movies/movies02.png
gallery3:    
  - url: images/docs/movies/movies03.png
    image_path: images/docs/movies/movies03.png    
gallery4:    
  - url: images/docs/movies/movies04.png
    image_path: images/docs/movies/movies04.png
gallery5:    
  - url: images/docs/movies/movies05.png
    image_path: images/docs/movies/movies05.png

---

The movie panel is the main panel in the movie management section. It consists of two main parts:

* Movie list (or often called movie table - left hand side)
* Movie information (right hand side). The information panel is divided into several sub-panels
  * Details - common movie details like title, release date, certifications, ...
  * Cast and crew
  * Media files - everything about the files of your movie
  * Artwork - a panel full with the artwork of your movie
  * Trailer - a list of all scraped trailers (URLs and trailer files)

{% include gallery id="gallery1" %}

## Movie List ##

The movie list part represents all movies in a table with some basic information as extra columns:

* Movie title - the sortable name affected by the prefixes from the settings; E.g. Bourne Legacy, The
* Original title
* Release year
* File name (of the main video file)
* Path (on the file system)
* Movie set title
* Rating (the main rating which you have chosen in the settings)
* Vote count (for the rating)
* Certification
* Date added
* Video format
* Aspect ratio
* HDR
* Audio channels
* Size of the video file(s)
* Edition
* Source (of the movie)
* 3D
* NFO - is there a NFO for this movie?
* Artwork - is there artwork for this movie?
* Trailer - is there a trailer for this movie?
* Subtitles - does this movie contain subtitles?
* Watched - has this movie been marked as watched

In addition to the movie list there are some more functions in this panel:

* Search for a movie (via title/original title)
* Filter/sort the movie list (via the popup which is launched from the "Filter" button)

## Movie Information ##

### Movie Details ###

{% include gallery id="gallery1" %}

The movie details panel shows the main artwork (poster and fanart) and holds all relevant data for the movie itself from various sources:

* Title and original title
* Release year and date
* Certification
* Ids in several movie databases (with direct links to the most common ones)
* Runtime
* Genres
* Rating (the _main_ or personal rating)
* Logo images for the most interesting facts of your movie file (certification, video and audio facts)
* Tagline and plot
* Production company and country
* Spoken languages (in the original version of the movie)
* The associated movie set
* The edition of the movie
* All set tags
* The path on the disk where the movie is
* Any special note for this movie

### Cast ###

{% include gallery id="gallery2" %}

The cast panel shows an overview over the:

* Directors
* Writers
* Producers
* Actors

of the movie. Actor entries may also contain an image of the person on the right hand side.

### Media Files ###

{% include gallery id="gallery3" %}

The media files panel shows all relevant data of your files for this movie. The upper area of this panel shows where your movie is located on the disk (path), when it has been added (date added) and if this movie has been watched yet (watched).

The next section shows the information for the _video_ stream of your movie:

* Source (origin of the movie file)
* Runtime (which is reported from the video file itself)
* Video codec
* Frame rate
* Video resolution
* Video bitrate
* Video bit depth

After the video section there is a list of all found _audio_ streams (inside the video file and external). This table shows various data about each audio stream such as:

* Source (inside the video file or external)
* Codec
* Bitrate
* Language

Beneath the table with the audio streams there is a table containing information of all found _subtitles_. This able only contains the location of the subtitle (internal/external) and the detected language.

In the bottom of this panel there is a table containing all _media files_ which has been found for this movie. This will be the movie file itself, any found audio and subtitle files, the NFO file and the artwork. For every media file there will be some data in this table like:

* Filename
* File size (in MB)
* Type of the media file
* Any detectable codec
* Video/image resolution
* Runtime
* Subtitle language

### Artwork ###

{% include gallery id="gallery4" %}

The artwork panel is a collection of all existing artwork for a movie.

### Trailer ###

{% include gallery id="gallery5" %}

The trailer panel shows a list of all trailers from the scraper (or NFO). You are able to watch the trailers from within this table or directly download it. At the moment there is only trailer per movie possible - if you decide to download a second one, the first one will be overwritten.
