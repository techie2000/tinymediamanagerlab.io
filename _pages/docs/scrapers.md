---
layout: single
permalink: "/docs/scrapers"
title: "Scrapers"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

# Scrapers #

## Meta Data Scrapers ##

tinyMediaManager offers a bunch of scrapers to get the meta data from various websites into your library. Those scrapers have different options which may help you to get the best possible result. At the moment tinyMediaManager offers scrapers for the following meta data sources:

* [themoviedb.org](/docs/scrapers#themoviedborg-tmdb)
* [imdb.com](/docs/scrapers#imdbcom-imdb)
* [moviemeter.nl](/docs/scrapers#moviemeternl)
* [mpdb.tv](/docs/scrapers#mpdbtv-mpdb)
* [trakt.tv](/docs/scrapers#trakttv-trakt)
* [omdbapi.com](/docs/scrapers#omdbapicom-omdb)
* [ofdb.de](/docs/scrapers#ofdbde-ofdb)
* [thetvdb.com](/docs/scrapers#thetvdbcom-tvdb)
* [anidb.net]

And additionally there are two meta scrapers which offer enhanced data:

* [Universal scraper](/docs/scrapers#universal-scraper): with this scraper you can combine results from various other scrapers to create an individual scraping result for your needs
* [Kodi scraper](/docs/scrapers#kodi-scraper): this meta scraper is able to parse scrapers from Kodi to embed them into tinyMediaManager. This scraper searches for local installed Kodi instances to use the scrapers from Kodi.

### themoviedb.org (TMDb) ###

[The Movie DB](https://www.themoviedb.org/) is the largest free movie database maintained by the community. Using this scraper you are able to get

* Movie meta data (multiple languages, including trailer links)
* Movie collection meta data (multiple languages)
* TV show meta data (multiple languages, including trailer links)
* Artwork
* Translated content

Our TMDb scraper offers the following settings:

* **API key**: you could enter your own API key here if you don't want to access TMDb data with the API key of tinyMediaManager
* **Include adult movies**: should the search include adult movies
* **Scrape language/country names**: scrape the names of spoken languages/countries rather than their ISO code
* **Also use the fallback language for title scraping**: when TMDb has no translated content in your preferred language you can choose another language to be used as fallback
* **Fallback language**

### imdb.com (IMDb) ###

The [Internet Movie Database](https://www.imdb.com/) is the most used database for movies all over the world. This online database has an excessive amount of data (which is constantly changing - even for older movies). IMDb offers only texts in English. Using this scraper you are able to get

* Movie meta data (English - multiple with TMDb activated)
* TV show meta data (English)

Our IMDb scraper offers the following settings:

* **Filter unwanted categories**: because IMDb offers data for most every movie (theatrical movies, shorts, TV productions, ...) you will probably have too much search results. Using this setting the search algorithm will only use the category "Movie/TV series" for searching
* **Use TMDb for movies**: because IMDb only offers English texts (like title, tagline, plot), non English speaking users probably want to activate this setting to fetch the translatable content from TMDb
* **Use TMDb for TV shows**: because IMDb only offers English texts (like title, tagline, plot), non English speaking users probably want to activate this setting to fetch the translatable content from TMDb
* **Scrape collection info from TMDb**: IMDb does not offer something like movie collections - with this setting the movie collection meta data will be fetched from TMDb
* **Get the release date for chosen country**: IMDb lists release dates for all countries (and even for re-releases) out there. With this setting you are able to fetch the release date for the country chosen in the scraper settings instead of getting the first release date from the movie
* **Scrape language/country names**: scrape the names of spoken languages/countries rather than their ISO code
* **Also parse keywords/tags**: IMDb offers a ton of keywords for every movie. Activating this setting forces the IMDb scraper to scrape all keywords

### moviemeter.nl ###

[Moviemeter.nl](https://www.moviemeter.nl/) is a dutch movie database. Using this scraper you are able to get

* Movie meta data (Dutch)

Our moviemeter.nl scraper offers the following settings:

* **API key**: you could enter your own API key here if you don't want to access moviemeter.nl data with the API key of tinyMediaManager
* **Scrape language/country names**: scrape the names of spoken languages/countries rather than their ISO code

### mpdb.tv (MPDb) ###

[MPDb.TV](http://www.mpdb.tv/) is a private meta data provider for French speaking users - you may need to become a member there to use this service (more infos at http://www.mpdb.tv/). Using this scraper you are able to get

* Movie meta data (French)

Our MPDb scraper offers the following settings:

* **Abo key**
* **Username**

Because MPDb is a private scraper you need the *abo key* and the *usernmame* from the MPDb site to logon to their service.

### trakt.tv (Trakt) ###

[Trakt.tv](https://trakt.tv) is a platform that does many things, but primarily keeps track of TV shows and movies you watch. It also provides meta data for movies and TV shows. Using this scraper you are able to get

* Movie meta data (English)
* TV show meta data (English)

Our Trakt scraper offers the following settings:

* **Client ID**: this is the "user" API key which is offered in your Trakt account. Use this if you don't want to use the Client ID of tinyMediaManager to scrape movies. This has no influence the the library synchronization which can also be done with Trakt

### omdbapi.com (OMDb) ###

[OMDb API](http://www.omdbapi.com/) is a web service to obtain movie information. All content and images on the site are contributed and maintained by our users. TinyMediaManager offers a limited access to OMDb (10 calls per 15 seconds) - if you want to use OMDb with more than this restricted access, you should become a patron of OMDb (https://www.patreon.com/join/omdb). Using this scraper you are able to get

* Movie meta data

Our OMDb scraper offers the following settings:

* **API key**: you could enter your own API key here if you don't want to access OMDb data with the API key of tinyMediaManager

### ofdb.de (OFDb) ###

The [Online Filmdatenbank](https://ssl.ofdb.de/) is a German movie database driven by the community. Using this scraper you are able to get

* Movie meta data (German)

### thetvdb.com (TVDB) ###

[The TVDB](https://thetvdb.com/) is an open database for television fans. This scraper is able to scrape TV series metadata and artwork. Using this scraper you are able to get

* TV show meta data (multiple languages)
* TV show artwork

* **API key**: you could enter your own API key here if you don't want to access TVDB data with the API key of tinyMediaManager
* **Fallback language for title/overview scraping**: when TVDB has no translated content in your preferred language you can choose another language to be used as fallback

### anidb.net (AniDB) ###

[AniDB](https://anidb.net/) stands for Anime DataBase. AniDB is a non-profit anime database that is open freely to the public. Using this scraper you are able to get

* TV show meta data (multiple languages, depends on the show)

### Universal Scraper ###

The Universal Scraper is a meta scraper which allows to collect and combine data from several other scrapers. It has support to combine the results of *TMDb*, *IMDb*, *Moviemeter.nl*, *Trakt* and *OMDb*. Using this scraper you are able to get

* Movie meta data (multiple, depends on the chosen scraper for translatable fields)

In the settings for the Universal Scraper you can specify for every field where is should be taken from.

**BE AWARE** that some combinations may not work. You should get the best results if you choose TMDb or IMDb for searching. Since this is just a meta scraper which calls the other scrapers, the settings for the other scraper are also taken into account and should be set.

### Kodi Scraper ###

The Kodi Scraper is also a meta scraper: it is able to parse XML scrapers (Python scrapers are not compatible) from a local Kodi installation to embed them into tinyMediaManager. This scraper searches for local installed Kodi instances to use the scrapers from Kodi. Using this scraper you are able to get:

* Movie meta data (multiple, depends on the chosen scraper)

Kodi installations will be searched in the following places:

Windows:

* Program Files
* Program Files (x86)
* Program Data
* your home folder (Kodi, .kodi, kodi, XMBC, .xbmc, xbmc)
* APPDATA (in your user profile)

macOS:

* /Applications/Kodi.app/Contents/Resources
* /Applications/XBMC.app/Contents/Resources
* your home folder (Kodi, .kodi, kodi, XMBC, .xbmc, xbmc)
* ~/Library/Application Support

Linux:

* /usr/share/
* /usr/lib/
* your home folder (Kodi, .kodi, kodi, XMBC, .xbmc, xbmc)

If you don't have any Kodi installed, but want to use the Kodi XML scrapers in your tinyMediaManager instance, you can also create a folder called `kodi_scraper` in your tinyMediaManager installation folder and put the scraper files **and all needed dependent scrapers/utils** from a Kodi installation into this folder. But remember: _tinyMediaManager cannot use Python based scrapers_.

If you encounter any issues with a Kodi scraper you should first check if the scraper works from within Kodi itself. If it does not work from within Kodi you should contact the developers of this Kodi scraper directly. If that scraper works inside Kodi there is probably a problem with parsing that scraper in tinyMediaManager.

## Artwork Scrapers ##

tinyMediaManager offers some artwork scrapers to get artwork for your media library. At the moment we offer the following scrapers:

* [themoviedb.org](/docs/scrapers#themoviedborg-tmdb-1)
* [fanart.tv](/docs/scrapers#fanarttv-fanart)
* [kyradb.com](/docs/scrapers#kyradbcom-kyradb)

### themoviedb.org (TMDb) ###

TMDb can also be used as an artwork scraper. For details see the section [above](/docs/scrapers#themoviedborg-tmdb).

### fanart.tv (fanart) ###

[Fanart.tv](https://fanart.tv/) provides a huge library of artwork for movies, TV shows and music. This service can be consumed with the API key tinyMediaManager offers, but if you want to have faster access to the artwork, you should become a [VIP at fanart.tv](https://fanart.tv/vip/).

Scraper settings:

* **Personal API key**: you could enter your own API key here if you don't want to access fanart.tv artwork with the API key of tinyMediaManager

### kyradb.com (kyradb) ###

[Kyradb.com](https://www.kyradb.com/) is a source for animated artwork.

Scraper settings:

* **API key**: you could enter your own API/User key here if you don't want to access kyradb artwork with the API key of tinyMediaManager
* **User key**
