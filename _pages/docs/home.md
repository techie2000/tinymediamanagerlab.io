---
layout: single
permalink: "/docs/"
title: "tinyMediaManager documentation"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---
# tinyMediaManager
tinyMediaManager ([https://www.tinymediamanager.org][2]) is a media management tool written in Java/Swing. It is written to provide metadata for Kodi (formerly known as XBMC). Due to the fact that it is written in Java, tinyMediaManager will run on Windows, Linux and Mac OSX (and possible more OS).

## How does tinyMediaManager work
The base work of tinyMediaManager is divided into three parts

1. Searching for media on your hard disks (aka _update data sources_ / importing) and adding it into an internal database
2. Searching for meta data in the internet (aka _scraping_)
3. Writing the found meta data onto the disk for further usage (aka _writing NFOs_)

## Where can I get tinyMediaManager
Download it here [https://www.tinymediamanager.org/download/][1]   
It will always auto-update to it's latest version (after confirmation).

## What do I need to run tinyMediaManager

### v4
* 64bit OS (Windows, macOS, Linux)
* RAM: at least 512MB free memory
* CPU: 1 GHz
* Screen Resolution: 1366 x 768 or above.

### v3
* Java Runtime Environment: Version 1.8 or higher
* RAM: at least 512MB free memory
* CPU: 1 GHz
* Screen Resolution: 1366 x 768 or above.

[1]: https://www.tinymediamanager.org/download/
[2]: https://www.tinymediamanager.org
