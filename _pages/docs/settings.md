---
layout: single
permalink: "/docs/settings"
title: "Settings"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---
You can access the settings via the menu "tinyMediaManager - Settings" on top of the tinyMediaManager window.

tinyMediaManager stores its settings in some files (`tmm.json`, `movies.json`, `tvshows.json`, ..) inside the folder `data` of your tinyMediaManager installation.

The settings are divided into several sections

* The section General Settings contains all "module independent" settings for tinyMediaManager like UI settings (language, font, theme, ..), recognized file types (video, audio, additional, ..) and system settings.
* [Movie Settings][1] contains all movie related settings for tinyMediaManager.
* [TV Show Settings][2] contains all TV show related settings for tinyMediaManager.

## General
In this section you can change some settings for the UI, system and connectivity:

### UI language ###
Select your preferred language for the UI. tinyMediaManager is translated to several different languages, if you feel that your desired language is missing or incomplete, please join us at [Weblate][3] to translate tinyMediaManager.

### UI Theme ###
tinyMediaManager offers a _light_ and a _dark_ theme.

### Font ###
This section offers settings for the UI font: you can choose the desired *font* and *font size* for tinyMediaManager.

Please be aware: tinyMediaManager is designed to use the font Dialog with a font size of 12. If you change the font/size there _might_ be some layout issues.

### Misc. settings ###
This section covers some other settings like _storing the window sizes/locations_, _default location of the image chooser dialog_ or _show the memory usage_.

* **Date Field**: Since the date added can be interpreted in several different ways, we introduced an own setting how to display this value in tinyMediaManager.
  * **Date added**: Show the _real_ date added (when the movie/TV show/episode) has been added to the database.
  * **File creation date**: Use the creation date of the video file of the movie/episode.
  * **File last modification date**: Use the date when the video file of the movie/episode has been modified the last time  .

* Default folder for tha image chooser when choosing local artwork.
  * **Last selected folder**: Open the last selected folder.
  * **Folder of the selected movie/TV show/episode**: Open the folder containing the selected movie/TV show/episode.

* **Store UI properties**: Store locations/sizes of the tinyMediaManager windows, selected/hidden columns, ...
* **Show memory usage**: Show the current memory usage in the status bar.

## File types ##

* **Video file types**
* **Subtitle and /additional file types**
* **Audio file types**

In these sections you can specify which file types should be analyzed when scanning for content.

### Unwanted file types ###

In this section you can enter file types to be found by the _cleanup unwanted files_ action. You can also use regular expressions to find unwanted files. Below are some of the Commands you can combine

| 1. | Common matching symbols |
|-----------|--
|`.`        | Matches any character
|`^regex`   | Finds regex that must match at the beginning of the line.
|`regex$`   | Finds regex that must match at the end of the line.
|`[abc]`    | Set definition, can match the letter a or b or c.
|`[abc][vz]`| Set definition, can match a or b or c followed by either v or z.
|`[^abc]`   | When a caret appears as the first character inside square brackets, it negates the pattern.
|`[a-d1-7]` | Ranges: matches a letter between a and d and figures from 1 to 7, but not d1.
|`X\|Z`     | Finds X or Z.
|`XZ`       | Finds X directly followed by Z.
|`$`        | Checks if a line end follows.

The following meta characters have a pre-defined meaning and make certain common patterns easier to use. For example, you can use \d as simplified definition for [0..9].

| 2. | Meta characters |
|--|--|
| `\d`      | Any digit, short for [0-9]|
| `\D`      | A non-digit, short for [^0-9]|
| `\s`      | A whitespace character, short for [ \t\n\x0b\r\f]|
| `\S`      | A non-whitespace character, short for|
| `\w`      | A word character, short for [a-zA-Z_0-9]|
| `\W`      | A non-word character [^\w]|
| `\S+`     | Several non-whitespace characters|
| `\b`      | Matches a word boundary where a word character is [a-zA-Z0-9_]|

A quantifier defines how often an element can occur. The symbols ?, \*, + and {} are qualifiers.

| 3. | Quantifiers | |
|--|--|--|
|`*`   |Occurs zero or more times, is short for {0,} | X* finds no or several letter X, .* finds any character sequence |
|`+`   |Occurs one or more times, is short for {1,} | X+- Finds one or several letter X |
|`?`   |Occurs no or one times, ? is short for {0,1}. | X? finds no or exactly one letter X |
|`{X}` |Occurs X number of times, {} describes the order of the preceding liberal | \d{3} searches for three digits, .{10} for any character sequence of length 10. |
|`{X,Y}`|Occurs between X and Y times, | \d{1,4} means \d must occur at least once and at a maximum of four. |
|`*?`  |? after a quantifier makes it a reluctant quantifier. It tries to find the smallest match. This makes the regular expression stop at the first match. |  |

## Title sorting ##

This Area contains a list of words/prefixes, which will moved from the beginning to the end of the movie, TV show and episode titles. For example:  

**The Bourne Identity** will become **Bourne Identity, The**  
This will now alphabetically sort in within "B".

If you do not want this feature to happen, just delete all entries from that list in settings...

## External devices ##

### Wake-on-LAN ###

You can enter a list of Wake-on-LAN devices which will be available for the action: `Tools > Wake-on-LAN`

### Kodi/XBMC ###

You can connect to a Kodi/XBMC device for remote function calls (experimental).

### UPnP ###

You can use tinyMediaManager as a UPnP server or remote play content on remote UPnP clients.

## External Services ##

### Trakt.tv ###

tinyMediaManager has a complete integration to the services Trakt.tv to synchronize your media library and watched states.

Trakt.tv uses a PIN auth to authorize external clients to connect to their service. First of all you need to create a Trakt.tv account. After that you can connect tinyMediaManager to your Trakt.tv account with pressing the button "Get PIN code for trakt.tc access". You will be redirected to the Trakt.tv website where you get your PIN. Enter this PIN in tinyMediaManager and you will be connected.

By clicking the button "Test connection to trakt.tv" you can test the connection to Trakt.tv

## System ##

### Media Player ###

Per default tinyMediaManager invokes the system standard media player to launch your video files. With this setting you can specify a media player which will be used for playing the files.

### FFmpeg

You can use FFmpeg to create custom thumbs/stills of you video files. To make this work, you have to specify where FFmpeg is installed or use the bundled FFmpeg addon.

### Memory settings ###

In this section you can specify the amount of memory which tinyMediaManager can use. Per default we use max 512MB - but with larger media libraries you may have to increase this setting.

### Proxy Settings ###

Here you can enter proxy settings, if you are using a proxy to connect to the internet. **Attention**: NTLM proxies do not work at the moment and you have to use something like [px-proxy][] or [CNTLM][2] to connect to the internet.

### HTTP API ###

Here you can enable the [HTTP API](/docs/http-api) to remote control tinyMediaManager:

* **Enable HTTP API**: Enable/disable the HTTP API
  * **HTTP port**: The port where the HTTP API listens to
  * **API key**: The API key you need to send with your requests

### Misc. settings ###

* **Parallel download count**: When downloading large files (like trailers) you can limit the amount of parallel downloads with this setting.
* **Disable SSL certificate verification**: sometimes something goes wrong with SSL certificates. If you encounter a unrecoverable problem with SSL certificates, you can disable the SSL certificate check here.

## Aspect Ratio Detector ##

tinyMediaManager can automatically detect the "real" aspect ratio of movies or TV shows. Many video files are encoded in 16:9 (1.78:1) aspect ratio even if the production was shot in a wider format (e.g. widescreen 2.40:1). In this case the video file includes black bars above and below the actual image. Without aspect ratio detection the video resolution and aspect ratio of the encoded video are included in media information data instead of the real values.

By using the aspect ratio detector feature tinyMediaManager will scan the real image area and will include the aspect ratio and the size of the cropped image (excluding black bars) in the media information.

Aspect ratio detection can be started in the drop-down menu of the toolbar's edit icon, in the edit module (Media files -> Detect aspect ratio), or in the context menu of the movie panel (Enhanced editing -> Detect aspect ratio of selected movie(s)/TV show(s)/episode(s)). Detection of video files in the .iso format is not possible.

To use this feature FFmpeg must be enabled in the [System settings][6].

* **Detection Mode**: To speed up detection several short samples of the video are analyzed (instead of scanning the complete video file). The samples are distributed over the video duration. If only few samples are analyzed and these samples contain dark scenes the detection might be less accurate.
  * **fast** - Only few samples are used. Less accurate detection.
  * **default** - More samples are used. Very good detection for all movies that do not have multiple formats.
  * **accurate** - At least 30 samples are used for detection. This is needed to detect multi format movies. This mode takes more time.

* **Round detected aspect ratio to**: The calculated aspect ratio is rounded to one of several user-selectable industry standard aspect ratios. You can limit the number of aspect ratios that will be used by deselecting the ones that are not relevant.
  * **Round to nearest aspect ratio** - The detected aspect ratio is rounded to the nearest aspect ratio that is selected.
  * **Round up to next wider aspect ratio** - The aspect ratio that is detected is rounded up the next wider (bigger) aspect ratio of the selected aspect ratios.

* **Detection of multi format videos**: Some movies have more than one aspect ratio: e.g. some scenes were filmed in 16:9 (1.78:1) format while others were shot in 70mm (2.20:1). You can choose how multi format videos should be detected. For a reliable detection more samples need to be analyzed. Multi format movies are only detected if _accurate_ detection mode is selected. If a multi format video is detected, in addition to the aspect ratio the second aspect ratio is included in the Edit / Media files tab. Also, you can enable a column that will display the 2nd aspect ratio in the movie/TV show list on the left side.
  * **Use most common aspect ratio** - Use the aspect ratio that is most frequently found. This will also be used if _fast_ or _default_ detection modes are selected.
  * **Use higher aspect ratio (e.g. for TV or 16:9 projection)** - The higher (narrower) aspect ratio (with the smaller aspect ratio value) is used. This is useful for TVs and projection on 16:9 screens: A scene with a "high" aspect ratio should fill the complete height of the screen. You will see format changes during the movie. Black bars will appear at the top and bottom in "wide" scenes. The complete image content is shown, and nothing is cropped.
  * **Use wider aspect ratio (e.g. for 21:9 projection)** - The wider aspect ratio (with the bigger aspect ratio value) is used. This is useful if you project on a 21:9 screen and you do not like to see any black bars: The image should be zoomed to fill the complete width of the screen. To avoid projecting "high" aspect ratio scenes outside of your screen the areas above and below the screen should be masked in the projector. Then you will watch a wide screen movie with a constant aspect ratio and without any format changes. But you will also miss some image content at the top and bottom in "high" aspect ratio scenes since this is cropped. This is how many movie theaters present multi format movies.

## Misc. settings ##

* **Enable image cache**: Image files which resist in your movie/TV show folders are sometimes huge and/or not accessible all the time (e.g. powered off NAS). With this option checked all loaded images will be cached locally (in the install folder), with a slightly lower resolution and quality. There is also a function, which will cache all images of all entries in your database (Debug -> Cache -> rebuild image cache). **Attention**: this is an "expensive" operation – it will take some time with maximum CPU load. After you have built the cache, all images will be shown in tinyMediaManager, even if the datasource is not reachable.
  * **Image size**: this sets the image size (pixels) of the cached image.
    * **SMALL** - scale images to a resolution of 400px long side. Useful for tinyMediaManager installations on devices with a low screen resolution (like HD/WXGA). Smallest image filesize.
    * **BIG** - scale images to a resolution of 1000px long side. Useful for tinyMediaManager installation on devices with a good  screen resolution (FHD, 2K, 4K). Noticeable bigger filesize.
    * **ORIGINAL** - do not scale images at all, just copy them to the cache folder to offline access. Probably huge filesize.
  * **Scaling quality**: this describes the algorithm of scaling/re encoding the cached images.
    * **BALANCED** -This scaling type tries to scale as good as possible as fast as possible. This will result in building the cache faster, but the results may not be as good as with the other options.
    * **QUALITY** - scale the images in high quality manner. This type should be used along with the SMALL cache image size to avoid artifacts. Using this cache type will take some time to build the cache.
    * **ULTRA_QUALITY** - scale the images with an even better algorithm for the highest possible quality. This cache type will need much processing time.

* **Delete trash/backup files folder on exit**: tinyMediaManager is using a trash/backup folder on every data source (.deletedByTMM) where deleted files and folders will be moved to. If you activate this setting, this folder will be deleted on shutdown of tinyMediaManager

* **Store mediainfo data in XML files next to your media files**: gathering media data can consume much time in several setups (like hosting your media files in the cloud). With this option, tinyMediaManager dumps the data if libmediainfo next to your media files (video files, isos, ...) so that recurring scans of your media files (like a re-build of the whole tinyMediaManager library) does not need to parse the media files again.

[1]: /docs/movies/settings
[2]: /docs/tvshows/settings
[3]: https://hosted.weblate.org/projects/tinymediamanager/translations/
[4]: https://github.com/genotrance/px
[5]: http://cntlm.sourceforge.net/
[6]: /docs/settings#ffmpeg
