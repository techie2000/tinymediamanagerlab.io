---
layout: single
permalink: "/docs/http-api"
title: "HTTP API"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "docs"
---

tinyMediaManager (v4.3+) offers an (experimental) HTTP API to remote control a running instance. To enable this API you need to go to the "Settings > General > System > HTTP API" and fill in the needed information:

* **Enable HTTP API**: Enable/disable the HTTP API
  * **HTTP port**: The port where the HTTP API listens to
  * **API key**: The API key you need to send with your requests (HTTP header field `api-key`)

After enabling the HTTP API you can send requests to the chosen port. Make sure you add the _API key_ in the header field `api-key` to all requests.

## Command structure

The HTTP API supports to send multiple commands via a `POST` request (JSON body) at once. Every command follows the same structure:

```
{
  "action": <action name>,
  "scope": {
    "name": <scope name>,
    "args": [
      <value>  
    ]
  },
  "args": {
    <key>: <value>  
  }
}
```

* **action**: The name of the action to trigger - you will find all implemented actions below
* **scope**: The scope for the action. This defines on which entries the action should be applied. The optional parameter `args` can be used to fine-tune the scope (not available on all scope values). Valid scope values depend on the action you trigger (details see below).
* **args**: Any extra arguments you may pass to the actions (optional - used by some actions)

When sending multiple commands, the order of execution may be different to the order in the request. tinyMediaManager makes sure the commands will be executed in the right order to preserve the best possible result.
{: .notice--info}

## Movies

These actions are executed in the movie module. To send a command to the movie module, you need to use the subpath `movie` (e.g. `http://localhost:7878/api/movie`).

### Update data sources

Trigger the _update data source_ action. This scans your data sources for new movies.

* **action** - `update`
* **scope**
  * `all`- update all data sources.
  * `single`- update a single data source. The indices for identifying the data source(s) will be used from **args** (multiple, starting with 0 for the first data source in your settings).

### Scrape

Scrape your movies from online sources.

* **action** - `scrape`
* **scope**
  * `new` or _empty_ - scrape all _new_ movies. Any movie which has been added in the last _update data source_ action will be classified as new.
  * `path`- scrape all movies from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- update all movies from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- scrape all movies.
* **args**
  * `scraper`(tmdb, imdb, tvdb, trakt, universal_movie, omdbapi, ofdb, moviemeter, mpdbtv, ...) - use the given scraper (optional, the scraper from the settings will be used if omitted).

### Trailer download

Download trailers.

* **action** - `downloadTrailer`
* **scope**
  * `new` or _empty_ - download trailers for all _new_ movies. Any movie which has been added in the last _update data source_ action will be classified as new.
  * `path`- download trailers for all movies from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- download trailers for all movies from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- download trailers for all movies.
* **args**
  * `onlyMissing` (true/false) - only download missing trailers (optional, defaults to true if omitted).

### Subtitle download  

Download subtitles.

* **action** - `downloadSubtitle`
* **scope**
  * `new` or _empty_ - download subtitles for all _new_ movies. Any movie which has been added in the last _update data source_ action will be classified as new.
  * `path`- download subtitles for all movies from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- download subtitles for all movies from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- download subtitles for all movies.
* **args**
  * `language` (ISO code) - the language to download the subtitles for (optional, defaults to the language of the settings if omitted).
  * `onlyMissing` (true/false) - only download missing subtitles (optional, defaults to true if omitted).

### Rename

Rename your movies according to your renamer patterns.

* **action** - `rename`
* **scope**
  * `new` or _empty_ - rename all _new_ movies. Any movie which has been added in the last _update data source_ action will be classified as new.
  * `path`- rename all movies from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- rename all movies from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- rename all movies.

### Export to template

Export your movies using an export template.

* **action** - `export`
* **scope**
  * `new` or _empty_ - export all _new_ movies. Any movie which has been added in the last _update data source_ action will be classified as new.
  * `path`- export all movies from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- export all movies from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- export all movies.
* **args**
  * `template` - the template name to use. Needs to be the folder name of the template.  
  * `exportPath` - path where to export your movies to.

## TV shows

These actions are executed in the TV show module. To send a command to the TV show module, you need to use the subpath `tvshow` (e.g. `http://localhost:7878/api/tvshow`).

### Update data sources

Trigger the _update data source_ action. This scans your data sources for new TV shows/episodes.

* **action** - `update`
* **scope**
  * `all`- update all data sources.
  * `single`- update a single data source. The indices for identifying the data source(s) will be used from **args** (multiple, starting with 0 for the first data source in your settings).

### Scrape

Scrape your TV shows/episodes from online sources.

* **action** - `scrape`
* **scope**
  * `new` or _empty_ - scrape all _new_ TV shows/episodes. All TV shows/episodes which have been added in the last _update data source_ action will be classified as new.
  * `path`- scrape all TV shows/episodes from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- update all TV shows/episodes from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- scrape all TV shows/episodes.

  ### Trailer download

Download trailers.

* **action** - `downloadTrailer`
* **scope**
  * `new` or _empty_ - download trailers for all _new_ TV shows. All TV shows which have been added in the last _update data source_ action will be classified as new.
  * `path`- download trailers for all TV shows from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- download trailers for all TV shows from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- download trailers for all TV shows.
* **args**
  * `onlyMissing` (true/false) - only download missing trailers (optional, defaults to true if omitted).

### Subtitle download  

Download subtitles.

* **action** - `downloadSubtitle`
* **scope**
  * `new` or _empty_ - download subtitles for all _new_ episodes. All episodes which have been added in the last _update data source_ action will be classified as new.
  * `path`- download subtitles for all episodes from the given TV show path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- download subtitles for all episodes from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- download subtitles for all episodes.
* **args**
  * `language` (ISO code) - the language to download the subtitles for (optional, defaults to the language of the settings if omitted).
  * `onlyMissing` (true/false) - only download missing subtitles (optional, defaults to true if omitted).

### Rename

Rename your TV shows/episodes according to your renamer patterns.

* **action** - `rename`
* **scope**
  * `new` or _empty_ - rename all _new_ TV shows/episodes. All TV shows/episodes which have been added in the last _update data source_ action will be classified as new.
  * `path`- rename all TV shows/episodes from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- rename all TV shows/episodes from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- rename all TV shows/episodes.

### Export to template

Export your TV shows using an export template.

* **action** - `export`
* **scope**
  * `new` or _empty_ - export all _new_ TV shows. All TV shows which has been added in the last _update data source_ action will be classified as new.
  * `path`- export all TV shows from the given path(s). You can pass the path(s) via **args** (multiple, no subpath possible).
  * `dataSource`- export all TV shows from the given data source(s). You can pass the data sources via **args** (multiple, either index of the data source or the path itself).
  * `all`- export all TV shows.
* **args**
  * `template` - the template name to use. Needs to be the folder name of the template.  
  * `exportPath` - path where to export your movies to.

## Examples

Update all movie data sources
```
curl -d '{"action":"update", "scope":{"name":"all"}}' -H "Content-Type: application/json" -H "api-key: cd496f0a-d879-4fcd-b766-12c59d3006f2" -X POST http://localhost:7878/api/movies
```

Update all TV show data sources AND scrape the new items
```
curl -d '[{"action":"update", "scope":{"name":"all"}},{"action":"scrape", "scope":{"name":"new"}}]' -H "Content-Type: application/json" -H "api-key: cd496f0a-d879-4fcd-b766-12c59d3006f2" -X POST http://localhost:7878/api/tvshows
```

Fetch subtitles for movies from our first data source (in French)
```
curl -d '{"action":"downloadSubtitle", "scope":{"name":"dataSource", "args":[0]}, "args":{"language":"fr"}}' -H "Content-Type: application/json" -H "api-key: cd496f0a-d879-4fcd-b766-12c59d3006f2" -X POST http://localhost:7878/api/movies
```