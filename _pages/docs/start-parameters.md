---
layout: single
permalink: "/docs/start-parameters"
title: "Start Parameters"

sidebar:
  nav: "docs"
---

In addition to the internal settings, tinyMediaManager also supports to set some low level runtime parameters through the launcher configuration. The launcher configuration (`launcher-extra.yml`) can contain either Java JVM parameters or some special parameters for tinyMediaManager itself. The launcher configuration must be in the form

```
javaHome: "custom Java home"
jvmOpts:
- "JVM - parameter"
env: 
- "environment option"
```

The sections of the configuration is as follows:

## javaHome

If you don't want to start tinyMediaManager with the shipped Java runtime (or the systems default Java runtime), you can enter the location of a custom Java runtime here.

Example:

```
javaHome: "/usr/lib/jvm/adoptopenjdk-14-hotspot-amd64/"
```

Default: `javaHome: ""`
{: .notice--warning}

## jvmOpts

Special runtime parameters for the JVM itself or for tinyMediaManager. Available options for tinyMediaManager are:

| Parameter | Description | Default |
|---|---|---|
| -Dtmm.mvstore.buffersize | Autocommit buffer size for MVStore in MB. | -Dtmm.mvstore.buffersize=8 |
| -Dtmm.legacy.filechooser | Enable the legacy swing file choosers | -Dtmm.legacy.filechooser=false |
| -Dtmm.uselibmediainfo | load libmediainfo | -Dtmm.uselibmediainfo=true |
| -Dtmm.contentfolder | Store all content (data, backups, cache, logs) in the chosen folder | - |
| -Dtmm.datafolder | Store data in the chosen folder | - |
| -Dtmm.cachefolder | Store cache in the chosen folder | - |
| -Dtmm.backupfolder | Store backups in the chosen folder | - |
| -Dtmm.logfolder | Store logs in the chosen folder | - |
| -Dtmm.noupdate | Disable automatic updates | false |
| -Dtmm.consoleloglevel | Log level for console output. Possible values: OFF, ERROR, WARN, INFO, DEBUG, TRACE | -Dtmm.consoleloglevel=DEBUG |

Common options for the JVM are:

| Parameter | Description | Default |
|---|---|---|
| -Xmx | Maximum heap (memory) size | -Xmx512m |

Example: 

```
jvmOpts:
  - "-Xmx1024m"
  - "-Dtmm.mvstore.buffersize=8"
```

Default: `jvmOpts: []`
{: .notice--warning}

## env

Some special environment options you may need to set. 

Common options for the JVM are:

| Parameter | Description | Default |
|---|---|---|
| `_JAVA_AWT_WM_NONREPARENTING` | Tell JVM your Window Manager is non-reparenting | - |

Example:

```
env:
  - "_JAVA_AWT_WM_NONREPARENTING=1"
```

Default: `env: []`
{: .notice--warning}
