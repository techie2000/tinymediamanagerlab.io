---
layout: single
permalink: "/help/faq-linux"
title: "Frequently Asked Questions - Linux"

toc: true
toc_label: "Table of Contents"
toc_icon: "cog"

sidebar:
  nav: "help"
---

## tinyMediaManager is unreadable with a high DPI monitor
For having high DPI support on Linux you need to have at least Java 9+ installed. If installing Java 9+ itself does not solve the issue, please follow the [Arch Linux Wiki](https://wiki.archlinux.org/index.php/HiDPI#Java_applications) to set the right JVM parameter - which can be set in the launcher config `launcher-extra.yml`:

```
jvmOpts:
  -Dsun.java2d.uiScale=2
```

**v4** ships its own Java (11+) - high DPI should be no more problem
{: .notice--primary}

**v3**: You can create (or edit if it already exists) a file called `extra.txt` in the tinyMediaManager install directory and add the following line `-Dsun.java2d.uiScale=2` to pass the JVM parameter to tinyMediaManager
{: .notice--warning}

## tinyMediaManager won't start
Some Linux distributions only provide a headless version of Java per default (this is the core part of Java without any UI libraries). Make sure you also have the UI part of Java installed. Find more details in the [Installation Page](/docs/installation).

**v4** ships its own Java - there is no need to install Java for tinyMediaManager
{: .notice--primary}

## libmediainfo does not load
libmediainfo is a native library which has to be compiled for every distribution/release/arch. We ship a pre packaged version of libmediainfo along with tinyMediaManager which should be suitable for most Linux users. If that version does not work for you, please try to install libmediainfo from your distribution (that is being loaded as fallback when the shipped one does not load). 

Find more details in the [Installation Page](/docs/installation).

Debian and Ubuntu may have rather old versions of libmediainfo in their repositories - please try to install libmediainfo directly from [https://mediaarea.net/de/MediaInfo/Download](https://mediaarea.net/de/MediaInfo/Download) (or include his own repository): 
{: .notice--primary}

## "Missing Software" is reported when trying to select a folder/file
We're using tinyFileDialogs for opening native file/folder choosers, but this library needs a native counterpart for the system to be called. In most Linux installations there is this counterpart already shipped, but for some setups you probabyl need to install the package `zenity` to provide this counterpart.

## Problems importing movies with special characters in their name
If you have problems to import movies with a special character in their name, check if the locale `en_US.UTF-8` has been generated on your system.
If not, have a look at the documentation of your linux distribution how to generate them. On Arch Linux/Manjaro the steps would be:

* login as root
* uncomment the line `en_US.UTF-8 UTF-8` in the file `/etc/locale.gen`
* run `locale-gen` as root
* logout and login again

## Ugly font rendering in Linux
Due to a bug in the JVM, fonts are drawn rather ugly in Linux desktop environments. We did as much improvements (with rendering parameters) as possible. If using a newer JVM (v1.8+) is not possible for you, there is the option to install a "patched JVM". There are packages for [Ubuntu](http://www.webupd8.org/2013/06/install-openjdk-patched-with-font-fixes.html) or [Arch Linux](https://aur.archlinux.org/packages/jre7-openjdk-infinality/). You may find similar packages for other Linux distributions as well.

**v4** ships its own Java (11+) where a better font rendering engine is included
{: .notice--primary}

## Blank window on startup
Users of non-re-parenting Window Managers (e.g. *XMonad*) might get a **blank window** (i.e. a window without anything in it) when starting up tinyMediaManager. This is because the JVM contains a hard-coded list of known non-re-parenting window managers, and certain window managers like XMonad are not included on the list.

Luckily, there are a couple of ways this can be mitigated:

The first thing to try is running tinyMediaManager with the environment variable `_JAVA_AWT_WM_NONREPARENTING` set:

```shell
_JAVA_AWT_WM_NONREPARENTING=1 ./tinyMediaManager
```

If that works, you can add this env var to your environment or [`launcher-extra.yml`](/docs/start-parameters).

If that doesn't work, you can try installing the **wmname** utility via your package manager, or from [https://tools.suckless.org/x/wmname](https://tools.suckless.org/x/wmname). Then, *before* you run the command `./tinyMediaManager`, run the command: `wmname LG3D`. This will instruct the shell to impersonate LG3D, the non-re-parenting window manager written in Java by Sun. Some ugly background info [here](https://web.archive.org/web/20161022025923/https://awesome.naquadah.org/wiki/Problems_with_Java).

You will have to do this each time before starting up tinyMediaManager, so it's probably easiest to add this into a shell script. For instance, add the following to a new file `tmm.sh` inside your tinyMediaManager directory:

```bash
#!/bin/bash
wmname LG3D
exec $(dirname)/tinyMediaManager
```

and run `chmod +x tmm.sh` once afterwards. 

To start tinyMediaManager, run this script (`./tmm.sh`) instead of `tinyMediaManager`.
{: .notice--primary}
